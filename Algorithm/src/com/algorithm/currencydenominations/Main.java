/**
 * 
 */
package com.algorithm.currencydenominations;

import java.util.Scanner;

/**
 * @author Tarang Gupta
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of currency denominations");
		int n = sc.nextInt();

		System.out.println("Enter the currency denominations value");
		int arr[] = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter the amount you want to pay");
		int amount = sc.nextInt();
		
		MergeSort mergeSort= new MergeSort();
		mergeSort.sort(arr, 0, arr.length-1);
		
				
		NoteCount noteCount= new NoteCount();
		noteCount.count(arr, amount);

		sc.close();

	}

}
