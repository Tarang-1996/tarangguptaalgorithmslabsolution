/**
 * 
 */
package com.algorithm.currencydenominations;

/**
 * @author Tarang Gupta
 *
 */
public class MergeSort {

	/**
	 * To make the sorted array
	 * 
	 * @param arr   
	 * array which need to be sorted
	 * 
	 * @param start 
	 * starting index of the array
	 * 
	 * @param end   
	 * last index of the array
	 * 
	 * @return void
	 */
	public void sort(int[] arr, int left, int right) {

		if (left < right) {
			int mid = left + (right - left) / 2;
			sort(arr, left, mid);
			sort(arr, mid + 1, right);
			merge(arr, left, right, mid);

		}
	}

	/**
	 * To merge the array in sorted order
	 * 
	 * @param arr   
	 * array which need to be sorted
	 * 
	 * @param start 
	 * starting index of the array
	 * 
	 * @param end   
	 * last index of the array
	 * 
	 * @param mid   
	 * middle index of the array
	 * 
	 * @return void
	 */
	public static void merge(int[] arr, int left, int right, int mid) {

		int len1 = mid - left + 1;
		int len2 = right - mid;

		int leftArr[] = new int[len1];
		int rightArr[] = new int[len2];

		for (int i = 0; i < len1; i++) {
			leftArr[i] = arr[left + i];
		}
		for (int i = 0; i < len2; i++) {
			rightArr[i] = arr[mid + 1 + i];
		}

		int i = 0;
		int j = 0;
		int k = left;

		while (i < len1 && j < len2) {
			if (leftArr[i] <= rightArr[j]) {
				arr[k++] = leftArr[i++];
			} else {
				arr[k++] = rightArr[j++];
			}
		}

		while (i < len1) {
			arr[k++] = leftArr[i++];
		}
		while (j < len2) {
			arr[k++] = rightArr[j++];
		}
	}

}
