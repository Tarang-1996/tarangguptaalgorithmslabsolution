package com.algorithm.currencydenominations;

/**
 * @author Tarang Gupta
 *
 */
public class NoteCount {

	/**
	 * To print the Denomination Note Count for given amount
	 * 
	 * @param arr    
	 * array of denomination of currency
	 * 
	 * @param amount 
	 * total amount to be paid
	 * 
	 * @return void
	 */
	public void count(int[] arr, int amount) {

		System.out.println("Your payment approach in order to give min no of notes will be: ");
		int sum = 0;

		for (int i = arr.length - 1; i >= 0; i--) {

			int count = 0;

			while (sum <= amount) {
				sum += arr[i];
				count++;
			}
			sum = sum - arr[i];
			count--;
			if (count > 0)
				System.out.println(arr[i] + ":" + count);
		}
	}

}
