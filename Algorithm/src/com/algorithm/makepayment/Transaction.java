package com.algorithm.makepayment;

import java.util.Scanner;

/**
 * @author Tarang Gupta
 *
 */

public class Transaction {
	
	/**
	 * to determine at which transaction target is achieved
	 * 
	 * @param arr   
	 * array containing number of transactions
	 * 
	 * @param target 
	 * daily target of transactions
	 * 
	 * @return int
	 * count of transactions
	 */

	public static int checkTarget(int[] arr, int target) {

		int sum = 0;
		int count = 0;

		for (int i = 0; i < arr.length; i++) {
			sum += arr[i];
			if (sum >= target) {

				count = i + 1;
				break;

			}
		}
		return count;
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("\n Enter the size of transaction array:");
		int n = sc.nextInt();
		
		int[] arr = new int[n];
		System.out.println("\n Enter the values of array: ");
		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}
		
		System.out.println("\n Enter the total no of targets that needs to be achieved: ");
		int targetNo = sc.nextInt();

		while (targetNo-- != 0) {

			System.out.println("Enter the value of target: ");
			int target = sc.nextInt();

			int c = checkTarget(arr, target);

			if (c > 0) {
				System.out.println("Target achieved  after" + c + " transactions");
			} else {
				System.out.println("Given target is not achieved");
			}

		}

		
		sc.close();
	}

}
